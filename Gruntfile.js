// Init Grunt modules
// ---------------------------------
module.exports = function(grunt) {

  // Grunt Time
  // ---------------------------------
  require('time-grunt')(grunt);

   // Load all tasks
  // ---------------------------------
  require('jit-grunt')(grunt);

  // Init Grunt config
  // ---------------------------------
  grunt.initConfig({

    // Define our source and build folders
    // ---------------------------------
    build:     '_public',
    css_build: '<%= build %>/stylesheet',
    js_build:  '<%= build %>/javascript',
    img_build: '<%= build %>/images',
    vendor:    '<%= build %>/vendors',

    src:       '_source',
    css_src:   '<%= src %>/stylus',
    js_src:    '<%= src %>/javascript',
    img_src:   '<%= src %>/images',
    svg_src:   '<%= src %>/svg',



    // Task: Stylus
    // ---------------------------------
    stylus: {
      dev: {
        options: {
          use: [
            require('jeet'),
            require('rupture'),
          ],
          compress: false,
          firebug: true,
          paths: [
          	'node_modules/grunt-contrib-stylus/node_modules',
            'node_modules/jeet/stylus',
            'node_modules/rupture',
            'node_modules/nib/lib',
            'node_modules/font-awesome-stylus/stylus'
          ]
        },
        files: {
          '<%= css_build %>/style.css': '<%= css_src %>/style.styl'
        }
      }
    },



    // Task: Agroup Media Querie
    // ---------------------------------
    combine_mq: {
      options: {
        log: true
      },
      dev: {
        files: {
          '<%= css_build %>/style.css': ['<%= css_build %>/style.css']
        }
      }
    },


    // CSSMin
    // ---------------------------------
    cssmin: {
      options: {
        shorthandCompacting: false,
        roundingPrecision: -1
      },
      target: {
        files: {
          '<%= css_build %>/style.css': '<%= css_build %>/style.css',
        }
      }
    },

    // Task: Ugligy
    // ---------------------------------
    uglify: {
    	vendor: {
        options: {
          mangle: false,
          compress : false,
          sourceMap: true,
          report: 'min'
        },
      	files: {
        	'<%= js_build %>/vendors.js': [
            	// Vendor Plugins
              'bower_components/jquery/dist/jquery.js', // jQuery
            ]
          }
        },
        dev: {
          options: {
            mangle: false,
            compress : true,
            sourceMap: true,
            report: 'min'
          },
          files: {
            '<%= js_build %>/app.js': [
            '<%= js_src %>/*.js',
            '<%= js_src %>/**/*.js',
          	]
          }
        },
    },


    // Task: JShint
    // ---------------------------------
    jshint: {
      ignore_warning: {
        options: {
          '-W033': true,
          '-W099': true,
        },
        src: ['<%= js_src %>/*.js', '<%= js_src %>/**/*.js', '<%= test %>/spec/*.js', '<%= test %>/spec/**/*.js'],
      },

    },


		// Task: Jade
		// ---------------------------------
		jade: {
			compile: {
				options: {
					client: false,
					preserveComments: false,
					pretty: true,
				},
				files: [{
					cwd: '<%= src %>/jade/',
					src: '*.jade',
					dest: '<%= build %>',
					expand: true,
					ext: '.html'
				}]
	    }
	  },

    // Task: Watch
    // ---------------------------------
    watch: {
      stylus: {
        files: [
          '<%= css_src %>/*.styl', '<%= css_src %>/**/*.styl'
        ],
        tasks: ['stylus', 'combine_mq', 'cssmin']
      },

      js: {
        files: ['<%= js_src %>/*.js', '<%= js_src %>/**/*.js'],
        tasks: ['uglify:dev']
      },

      html: {
        files: ['<%= src %>/*.jade', '<%= src %>/**/*.jade'],
        //tasks: ['newer:jade']
        tasks: ['jade']
      },

      build: {
        files: [
          'Gruntfile.js'
        ],
        tasks: ['build']
      }
    },


    // Task: BrowserSync
    // ---------------------------------
    browserSync: {
      dev: {
        options: {
          files : [
            '<%= css_build %>/*.css',
            '<%= js_build %>/*.js',
            '<%= build %>/**/*.jpg',
            '<%= build %>/**/*.png',
            '<%= build %>/**/*.svg',
            '<%= build %>/**/*.html'
          ],

          host : '',

          server: {
            baseDir: '<%= build %>/',
            directory: true
          },

          watchTask: true,

          ghostMode: {
            clicks: true,
            scroll: true,
            links: true,
            forms: true
          }
        }
      }
    }

  });


  // Grunt registers
  // ---------------------------------

  // Stylus
  grunt.registerTask( 'styl', ['stylus'] );

  // Js
  grunt.registerTask( 'js', ['jshint', 'uglify']);

  // CSS
  grunt.registerTask( 'css', ['stylus', 'combine_mq', 'cssmin']);

  // Build
  grunt.registerTask( 'build', ['jshint', 'uglify', 'jade', 'stylus', 'combine_mq', 'cssmin' ] );

  // Watch
  grunt.registerTask( 'w', ['browserSync', 'watch' ] );

};
